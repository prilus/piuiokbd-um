#include <thread>
#include <deque>
#include <mutex>
#include <iostream>
#include <thread>

#include <Windows.h>
#include <stdint.h>

#include "common.h"

using namespace std;

enum {
	QUEUE_MAX = 1000,
};

struct key_event {
	uint32_t tick;
	uint32_t bit;
};

struct key_mapping {
	uint32_t bit;
	uint8_t vkCode;
	uint8_t scanCode;
};

const key_mapping ioKeyCodeTable[] = {
	{0x00000001, 'Q', 0x10}, 	// 1p left top,		q
	{0x00000002, 'E', 0x12}, 	// 1p right top,	e
	{0x00000004, 'S', 0x1F}, 	// 1p center, 		s
	{0x00000008, 'Z', 0x2C}, 	// 1p left bottom, 	z
	{0x00000010, 'C', 0x2E}, 	// 1p right bottom, c

	{0x00010000, 'R', 0x13}, 	// 2p left top, 	r
	{0x00020000, 'Y', 0x15}, 	// 2p right top, 	y
	{0x00040000, 'G', 0x22}, 	// 2p center, 		g
	{0x00080000, 'V', 0x2F}, 	// 2p left bottom, 	v
	{0x00100000, 'N', 0x31}, 	// 2p right bottom, n

	{0x00004000, '1', 0x02}, 	// service, 		1
	{0x00000200, '2', 0x03}, 	// test, 			2
	{0x00000100, '3', 0x04}, 	// clear, 			3
};

constexpr int ioKeyCodeTableSize = sizeof(ioKeyCodeTable) / sizeof(*ioKeyCodeTable);

const auto queue = new deque<key_event>();
const auto queueLock = new mutex();

uint32_t isRunning = 1;
thread* tkey = nullptr;

// config
const auto iniFilename = L".\\piuiokbd.ini";

uint32_t keyDelay = 0;

void sendKey(uint32_t prev, uint32_t cur) {
	static INPUT keyData[ioKeyCodeTableSize];

	if (prev == cur) {
		return;
	}

	auto tick = (uint32_t)GetTickCount64();
	int eventCount = 0;

	for (int i = 0; i < ioKeyCodeTableSize; i++) {
		auto key = &keyData[eventCount];
		auto mapping = &ioKeyCodeTable[i];
		auto bit = mapping->bit;

		if ((prev & bit) != (cur & bit)) {
			key->type = INPUT_KEYBOARD;
			key->ki.wVk = mapping->vkCode;
			key->ki.wScan = mapping->scanCode;
			key->ki.dwFlags = (cur & bit) ? 0 : KEYEVENTF_KEYUP;
			key->ki.time = tick;

			eventCount++;
		}
	}

	if (0 == eventCount) {
		return; // ???
	}

	auto n = SendInput(eventCount, keyData, sizeof(INPUT));
}


void keyEventThread() {
	uint32_t prev = 0;

#ifdef _DEBUG
	cout << "start key event thread" << endl;
#endif

	while (true) {
		this_thread::sleep_for(1ms);

		if (!InterlockedCompareExchange(&isRunning, 0, 0)) {
#ifdef _DEBUG
			cout << "end key event thread" << endl;
#endif
			return;
		}

		auto tick = (uint32_t)GetTickCount64();

		if (!queueLock->try_lock()) {
			// busy
			continue;
		}

		if (queue->empty()) {
			queueLock->unlock();
			continue;
		}

		auto ev = queue->front();

		if (tick < ev.tick) {
			queueLock->unlock();
			continue;
		}

		queue->pop_front();
		queueLock->unlock();

		sendKey(prev, ev.bit);

		prev = ev.bit;
	}
}

void addKeyEvent(uint32_t bit) {
	auto nextTick = (uint32_t)(GetTickCount64() + keyDelay);

	queueLock->lock();

	if (QUEUE_MAX <= queue->size()) {
		// ?
		queueLock->unlock();

		system("pause");
		exit(1);
	}

	queue->push_back(key_event{ nextTick, bit });

	queueLock->unlock();
}

void initKeyEvent() {
	tkey = new thread(keyEventThread);

	// read key delay
	{
		auto newKeyDelay = GetPrivateProfileInt(L"CONFIG", L"KEY_DELAY", 0, iniFilename);
		keyDelay = newKeyDelay;

		cout << "set key delay : " << newKeyDelay << endl;
	}
}

void finiKeyEvent() {
	InterlockedExchange(&isRunning, 0);

	if (tkey) {
		tkey->join();

		tkey = nullptr;
	}
}