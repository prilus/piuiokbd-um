#include <iostream>
#include <thread>
#include <cstdint>
#include <Windows.h>

#include "libusb.h"
#include "common.h"

using namespace std;

/*
lx io

in (controller -> pc)
1
4 - 1p 3r
5 - 1p 1r
6 - 1p 5r
7 - 1p 9r
8 - 1p 7r

2
4 - 1p 3l
5 - 1p 1l
6 - 1p 5l
7 - 1p 9l
8 = 1p 7l

3
4 - 1p 3b
5 - 1p 1b
6 - 1p 5b
7 - 1p 9b
8 - 1p 7b

4
4 - 1p 3t
5 - 1p 1t
6 - 1p 5t
7 - 1p 9t
8 - 1p 7t

5678 2p

9
1 - clear
2 - service
7 - test

11 hand 1p
12 hand 2p

out (pc -> controller)
led
1
2 - 1p 3
3 - 1p 1
4 - 1p 5
5 - 1p 9
6 - 1p 7

3
2 - 2p 3
3 - 2p 1
4 - 2p 5
5 - 2p 9
6 - 2p 7

5
4 - 1p 3
5 - 1p 1
6 - 1p 5
7 - 1p 9
8 - 1p 7

6
4 - 2p 3
5 - 2p 1
6 - 2p 5
7 - 2p 9
8 - 2p 7

*/

enum {
	// old io
	_OLD_VID = 0x0547,
	_OLD_PID = 0x1002,

	// lx io
	_NEW_VID = 0x0D2F,
	_NEW_PID = 0x1020,

	// test keyboard
	//_VID = 0x04B4,
	//_PID = 0x0101,

	_SENSOR_RIGHT = 0x0ULL,
	_SENSOR_LEFT = 0x10001ULL,
	_SENSOR_BOTTOM = 0x20002ULL,
	_SENSOR_TOP = 0x30003ULL,

	_LED_MASK = 0x1F001FULL,
	_LED_LSHIFT = 2,

	HID_GET_REPORT = 0x01,
	HID_SET_REPORT = 0x09,
	HID_REPORT_TYPE_INPUT = 0x01,
	HID_REPORT_TYPE_OUTPUT = 0x02,
};


void libusb_print_version() {
	auto version = libusb_get_version();

	cout << "libusb version "
		<< version->major << "."
		<< version->minor << "."
		<< version->micro << "."
		<< version->nano << endl;
}

int pushIO(libusb_device_handle* handle, uint64_t data) {
	auto reqType = LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_VENDOR;
	auto r = libusb_control_transfer(handle, reqType, 0xae, 0, 0, (unsigned char*)&data, sizeof(data), 5000);
	if (0 > r) {
		cout << "usb push data error " << r << endl;
		return -1;
	}

	return 0;
}

uint64_t pullIO(libusb_device_handle* handle) {
	uint64_t data{};

	auto reqType = LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_VENDOR;
	auto r = libusb_control_transfer(handle, reqType, 0xae, 0, 0, (unsigned char*)&data, sizeof(data), 5000);
	if (0 > r) {
		cout << "usb pull data error " << r << endl;
		return -1;
	}

	return data;
}

void pushReport(libusb_device_handle* handle, uint8_t* data, size_t dataSize) {
	auto reqType = LIBUSB_ENDPOINT_OUT | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE;
	auto r = libusb_control_transfer(handle, reqType, HID_SET_REPORT, (HID_REPORT_TYPE_OUTPUT << 8) | 0, 0, data, dataSize, 5000);

	if (0 > r) {
		cout << "usb push report data error " << r << endl;

		if (r == LIBUSB_ERROR_PIPE)
			libusb_clear_halt(handle, 0);

		return;
	}
}

void pullReport(libusb_device_handle* handle, uint8_t* data, size_t dataSize) {
	auto reqType = LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_CLASS | LIBUSB_RECIPIENT_INTERFACE;
	auto r = libusb_control_transfer(handle, reqType, HID_GET_REPORT, (HID_REPORT_TYPE_INPUT << 8) | 0, 0, data, dataSize, 5000);

	if (0 > r) {
		cout << "usb pull report data error " << r << endl;

		if (r == LIBUSB_ERROR_PIPE)
			libusb_clear_halt(handle, 0);

		return;
	}
}


void pollingOldIO() {
	auto handle = libusb_open_device_with_vid_pid(nullptr, _OLD_VID, _OLD_PID);
	if (!handle) {
		cout << "usb open error" << endl;
		return;
	}

	cout << "usb handle opened" << endl;

	uint64_t cur{};
	uint64_t led{};

	uint8_t ledtest[16] = { 0, };

	while (true) {
		this_thread::sleep_for(1ms);

		pushIO(handle, led | _SENSOR_LEFT);
		auto key_left = pullIO(handle);

		pushIO(handle, led | _SENSOR_RIGHT);
		auto key_right = pullIO(handle);

		pushIO(handle, led | _SENSOR_BOTTOM);
		auto key_bottom = pullIO(handle);

		pushIO(handle, led | _SENSOR_TOP);
		auto key_top = pullIO(handle);

		if (-1 == key_left || -1 == key_right || -1 == key_bottom || -1 == key_top)
			continue;

		auto data = key_left & key_right & key_bottom & key_top;

		cur = (~data & _LED_MASK);
		led = cur << _LED_LSHIFT;

		addKeyEvent(cur);
	}
}

void pollingNewIO() {
	auto handle = libusb_open_device_with_vid_pid(nullptr, _NEW_VID, _NEW_PID);
	if (!handle) {
		cout << "usb open error" << endl;
		return;
	}

	cout << "usb handle opened" << endl;

	uint32_t cur{ 0 };
	uint8_t sensors[16]{ 0, };
	uint8_t led[16]{ 0, };

	while (true) {
		this_thread::sleep_for(1ms);

		pullReport(handle, sensors, sizeof(sensors));

		//						 right 		left 			bottom		top
		uint8_t sensors_1p = ~(sensors[0] & sensors[1] & sensors[2] & sensors[3]);
		uint8_t sensors_2p = ~(sensors[4] & sensors[5] & sensors[6] & sensors[7]);
		uint8_t sensors_etc = ~sensors[8];

		uint8_t sensors_hand1p = ~sensors[10];
		uint8_t sensors_hand2p = ~sensors[11];

		cur = sensors_1p | ((uint32_t)sensors_2p) << 16 | ((uint32_t)sensors_etc) << 8 |
			sensors_hand1p | ((uint32_t)sensors_hand2p) << 16;

		led[0] = sensors_1p << _LED_LSHIFT;
		led[2] = sensors_2p << _LED_LSHIFT;

		led[4] = sensors_hand1p;
		led[5] = sensors_hand2p;

		pushReport(handle, led, sizeof(led));

		addKeyEvent(cur);
	}
}

int main(int argc, char** argv) {
	cout << "piuiokbd-um ( https://gitlab.com/prilus/piuiokbd-um ), build date: " << __DATE__ " " __TIME__ << endl;

	libusb_init(nullptr);
	//libusb_set_option(nullptr, LIBUSB_OPTION_LOG_LEVEL, LIBUSB_LOG_LEVEL_DEBUG);
	libusb_print_version();
	
	initKeyEvent();
	
	pollingOldIO();
	pollingNewIO();

	libusb_exit(nullptr);

	finiKeyEvent();

	system("pause");

	return 0;
}

/*
void display_buffer_hex(uint8_t* buffer, size_t size)
{
	unsigned i, j, k;

	for (i = 0; i < size; i += 16) {
		printf("\n  %08x  ", i);
		for (j = 0, k = 0; k < 16; j++, k++) {
			if (i + j < size) {
				printf("%02x", buffer[i + j]);
			}
			else {
				printf("  ");
			}
			printf(" ");
		}
		printf(" ");
		for (j = 0, k = 0; k < 16; j++, k++) {
			if (i + j < size) {
				if ((buffer[i + j] < 32) || (buffer[i + j] > 126)) {
					printf(".");
				}
				else {
					printf("%c", buffer[i + j]);
				}
			}
		}
	}
	printf("\n");
}
*/

/*
static int get_hid_record_size(uint8_t* hid_report_descriptor, int size, int type)
{
	uint8_t i, j = 0;
	uint8_t offset;
	int record_size[3] = { 0, 0, 0 };
	int nb_bits = 0, nb_items = 0;
	bool found_record_marker;

	found_record_marker = false;
	for (i = hid_report_descriptor[0] + 1; i < size; i += offset) {
		offset = (hid_report_descriptor[i] & 0x03) + 1;
		if (offset == 4)
			offset = 5;
		switch (hid_report_descriptor[i] & 0xFC) {
		case 0x74:	// bitsize
			nb_bits = hid_report_descriptor[i + 1];
			break;
		case 0x94:	// count
			nb_items = 0;
			for (j = 1; j < offset; j++) {
				nb_items = ((uint32_t)hid_report_descriptor[i + j]) << (8 * (j - 1));
			}
			break;
		case 0x80:	// input
			found_record_marker = true;
			j = 0;
			break;
		case 0x90:	// output
			found_record_marker = true;
			j = 1;
			break;
		case 0xb0:	// feature
			found_record_marker = true;
			j = 2;
			break;
		case 0xC0:	// end of collection
			nb_items = 0;
			nb_bits = 0;
			break;
		default:
			continue;
		}
		if (found_record_marker) {
			found_record_marker = false;
			record_size[j] += nb_items * nb_bits;
		}
	}
	if ((type < HID_REPORT_TYPE_INPUT) || (type > HID_REPORT_TYPE_FEATURE)) {
		return 0;
	}
	else {
		return (record_size[type - HID_REPORT_TYPE_INPUT] + 7) / 8;
	}
}
*/


/*
static int test_device(libusb_device_handle* handle)
{
	libusb_device* dev;
	uint8_t bus, port_path[8];
	struct libusb_bos_descriptor* bos_desc;
	struct libusb_config_descriptor* conf_desc;
	const struct libusb_endpoint_descriptor* endpoint;
	int i, j, k, r;
	int iface, nb_ifaces, first_iface = -1;
	struct libusb_device_descriptor dev_desc;
	const char* const speed_name[6] = { "Unknown", "1.5 Mbit/s (USB LowSpeed)", "12 Mbit/s (USB FullSpeed)",
		"480 Mbit/s (USB HighSpeed)", "5000 Mbit/s (USB SuperSpeed)", "10000 Mbit/s (USB SuperSpeedPlus)" };
	char string[128];
	uint8_t string_index[3];	// indexes of the string descriptors
	uint8_t endpoint_in = 0, endpoint_out = 0;	// default IN and OUT endpoints

	dev = libusb_get_device(handle);
	bus = libusb_get_bus_number(dev);
	//if (extra_info) {
	//	r = libusb_get_port_numbers(dev, port_path, sizeof(port_path));
	//	if (r > 0) {
	//		printf("\nDevice properties:\n");
	//		printf("        bus number: %d\n", bus);
	//		printf("         port path: %d", port_path[0]);
	//		for (i = 1; i < r; i++) {
	//			printf("->%d", port_path[i]);
	//		}
	//		printf(" (from root hub)\n");
	//	}
	//	r = libusb_get_device_speed(dev);
	//	if ((r < 0) || (r > 5)) r = 0;
	//	printf("             speed: %s\n", speed_name[r]);
	//}

	printf("\nReading device descriptor:\n");
	libusb_get_device_descriptor(dev, &dev_desc);
	//CALL_CHECK_CLOSE(libusb_get_device_descriptor(dev, &dev_desc), handle);
	printf("            length: %d\n", dev_desc.bLength);
	printf("      device class: %d\n", dev_desc.bDeviceClass);
	printf("               S/N: %d\n", dev_desc.iSerialNumber);
	printf("           VID:PID: %04X:%04X\n", dev_desc.idVendor, dev_desc.idProduct);
	printf("         bcdDevice: %04X\n", dev_desc.bcdDevice);
	printf("   iMan:iProd:iSer: %d:%d:%d\n", dev_desc.iManufacturer, dev_desc.iProduct, dev_desc.iSerialNumber);
	printf("          nb confs: %d\n", dev_desc.bNumConfigurations);
	// Copy the string descriptors for easier parsing
	string_index[0] = dev_desc.iManufacturer;
	string_index[1] = dev_desc.iProduct;
	string_index[2] = dev_desc.iSerialNumber;

	//printf("\nReading BOS descriptor: ");
	//if (libusb_get_bos_descriptor(handle, &bos_desc) == LIBUSB_SUCCESS) {
	//	printf("%d caps\n", bos_desc->bNumDeviceCaps);
	//	for (i = 0; i < bos_desc->bNumDeviceCaps; i++)
	//		print_device_cap(bos_desc->dev_capability[i]);
	//	libusb_free_bos_descriptor(bos_desc);
	//}
	//else {
	//	printf("no descriptor\n");
	//}

	printf("\nReading first configuration descriptor:\n");
	libusb_get_config_descriptor(dev, 0, &conf_desc);
	//CALL_CHECK_CLOSE(libusb_get_config_descriptor(dev, 0, &conf_desc), handle);
	printf("              total length: %d\n", conf_desc->wTotalLength);
	printf("         descriptor length: %d\n", conf_desc->bLength);
	nb_ifaces = conf_desc->bNumInterfaces;
	printf("             nb interfaces: %d\n", nb_ifaces);
	if (nb_ifaces > 0)
		first_iface = conf_desc->interface[0].altsetting[0].bInterfaceNumber;
	for (i = 0; i < nb_ifaces; i++) {
		printf("              interface[%d]: id = %d\n", i,
			conf_desc->interface[i].altsetting[0].bInterfaceNumber);
		for (j = 0; j < conf_desc->interface[i].num_altsetting; j++) {
			printf("interface[%d].altsetting[%d]: num endpoints = %d\n",
				i, j, conf_desc->interface[i].altsetting[j].bNumEndpoints);
			printf("   Class.SubClass.Protocol: %02X.%02X.%02X\n",
				conf_desc->interface[i].altsetting[j].bInterfaceClass,
				conf_desc->interface[i].altsetting[j].bInterfaceSubClass,
				conf_desc->interface[i].altsetting[j].bInterfaceProtocol);
			if ((conf_desc->interface[i].altsetting[j].bInterfaceClass == LIBUSB_CLASS_MASS_STORAGE)
				&& ((conf_desc->interface[i].altsetting[j].bInterfaceSubClass == 0x01)
					|| (conf_desc->interface[i].altsetting[j].bInterfaceSubClass == 0x06))
				&& (conf_desc->interface[i].altsetting[j].bInterfaceProtocol == 0x50)) {
				// Mass storage devices that can use basic SCSI commands
				//test_mode = USE_SCSI;
			}
			for (k = 0; k < conf_desc->interface[i].altsetting[j].bNumEndpoints; k++) {
				struct libusb_ss_endpoint_companion_descriptor* ep_comp = NULL;
				endpoint = &conf_desc->interface[i].altsetting[j].endpoint[k];
				printf("       endpoint[%d].address: %02X\n", k, endpoint->bEndpointAddress);
				// Use the first interrupt or bulk IN/OUT endpoints as default for testing
				if ((endpoint->bmAttributes & LIBUSB_TRANSFER_TYPE_MASK) & (LIBUSB_TRANSFER_TYPE_BULK | LIBUSB_TRANSFER_TYPE_INTERRUPT)) {
					if (endpoint->bEndpointAddress & LIBUSB_ENDPOINT_IN) {
						if (!endpoint_in)
							endpoint_in = endpoint->bEndpointAddress;
					}
					else {
						if (!endpoint_out)
							endpoint_out = endpoint->bEndpointAddress;
					}
				}
				printf("           max packet size: %04X\n", endpoint->wMaxPacketSize);
				printf("          polling interval: %02X\n", endpoint->bInterval);
				libusb_get_ss_endpoint_companion_descriptor(NULL, endpoint, &ep_comp);
				if (ep_comp) {
					printf("                 max burst: %02X   (USB 3.0)\n", ep_comp->bMaxBurst);
					printf("        bytes per interval: %04X (USB 3.0)\n", ep_comp->wBytesPerInterval);
					libusb_free_ss_endpoint_companion_descriptor(ep_comp);
				}
			}
		}
	}
	libusb_free_config_descriptor(conf_desc);

	libusb_set_auto_detach_kernel_driver(handle, 1);
	//for (iface = 0; iface < nb_ifaces; iface++)
	//{
	//	int ret = libusb_kernel_driver_active(handle, iface);
	//	printf("\nKernel driver attached for interface %d: %d\n", iface, ret);
	//	printf("\nClaiming interface %d...\n", iface);
	//	r = libusb_claim_interface(handle, iface);
	//	if (r != LIBUSB_SUCCESS) {
	//		printf("   Failed.\n");
	//		return;
	//	}
	//}

	printf("\nReading string descriptors:\n");
	for (i = 0; i < 3; i++) {
		if (string_index[i] == 0) {
			continue;
		}
		if (libusb_get_string_descriptor_ascii(handle, string_index[i], (unsigned char*)string, sizeof(string)) > 0) {
			printf("   String (0x%02X): \"%s\"\n", string_index[i], string);
		}
	}

	//printf("\nReading OS string descriptor:");
	//r = libusb_get_string_descriptor(handle, MS_OS_DESC_STRING_INDEX, 0, (unsigned char*)string, MS_OS_DESC_STRING_LENGTH);
	//if (r == MS_OS_DESC_STRING_LENGTH && memcmp(ms_os_desc_string, string, sizeof(ms_os_desc_string)) == 0) {
	//	// If this is a Microsoft OS String Descriptor,
	//	// attempt to read the WinUSB extended Feature Descriptors
	//	printf("\n");
	//	read_ms_winsub_feature_descriptors(handle, string[MS_OS_DESC_VENDOR_CODE_OFFSET], first_iface);
	//}
	//else {
	//	printf(" no descriptor\n");
	//}

	return 0;
}
*/

/*
int pullDesc(libusb_device_handle* handle) {
	uint8_t data[0x100]{};

	auto reqType = LIBUSB_ENDPOINT_IN | LIBUSB_REQUEST_TYPE_STANDARD | LIBUSB_RECIPIENT_INTERFACE;
	auto r = libusb_control_transfer(handle, reqType, LIBUSB_REQUEST_GET_DESCRIPTOR, LIBUSB_DT_REPORT << 8, 0, (unsigned char*)&data, sizeof(data), 5000);
	if (0 > r) {
		cout << "usb push data error " << r << endl;
		return -1;
	}

	display_buffer_hex(data, r);

	return get_hid_record_size(data, r, HID_REPORT_TYPE_INPUT);
}
*/